################## fao cross correlation overview ##################

1. [FAO_NatCropArea](crpar) and [FAO_NatCropProd](crppr)
               year cross_correlation p_value
1              2000         0.8192130       0
2              2001         0.8222482       0
3              2002         0.8056018       0
4              2003         0.8042192       0
5              2004         0.8172928       0
6              2005         0.8152881       0
7              2006         0.8058125       0
8              2007         0.8012322       0
9              2008         0.8015888       0
10             2009         0.7997271       0
11             2010         0.7949699       0
12             2011         0.7931202       0
13             2012         0.8358428       0
14             mean         0.8089351      NA
15 mean_significant         0.8089351      NA


2. [FAO_NatFertilizer](natfrt) and [FAO_NatCropProd](crppr)
               year cross_correlation p_value
1              2002         0.9824696       0
2              2003         0.9844789       0
3              2004         0.9865330       0
4              2005         0.9820055       0
5              2006         0.9778745       0
6              2007         0.9825977       0
7              2008         0.9778438       0
8              2009         0.9692010       0
9              2010         0.9761906       0
10             2011         0.9818610       0
11             mean         0.9801056      NA
12 mean_significant         0.9801056      NA


3. [FAO_AnimalProd](anipr) and [FAO_ProducingAnimals](prani)
               year cross_correlation p_value
1              2000         0.9070349       0
2              2001         0.9061888       0
3              2002         0.9063160       0
4              2003         0.9082711       0
5              2004         0.9071018       0
6              2005         0.9072120       0
7              2006         0.9070931       0
8              2007         0.9108442       0
9              2008         0.9103166       0
10             2009         0.9110405       0
11             2010         0.9092159       0
12             2011         0.9106830       0
13             mean         0.9084431      NA
14 mean_significant         0.9084431      NA


4. [FAO_AnimalProd](anipr) and [FAO_NatAnimals](natani)
               year cross_correlation p_value
1              2000         0.6939574       0
2              2001         0.7014465       0
3              2002         0.7036022       0
4              2003         0.7154647       0
5              2004         0.7262312       0
6              2005         0.7318936       0
7              2006         0.7458537       0
8              2007         0.7529722       0
9              2008         0.7538611       0
10             2009         0.7637965       0
11             2010         0.7655522       0
12             2011         0.7669738       0
13             mean         0.7351338      NA
14 mean_significant         0.7351338      NA


5. [FAO_ProducingAnimals](prani) and  [FAO_NatAnimals](natani)
               year cross_correlation p_value
1              2000         0.8772355       0
2              2001         0.8832537       0
3              2002         0.8815981       0
4              2003         0.8792636       0
5              2004         0.8787969       0
6              2005         0.8811982       0
7              2006         0.8839134       0
8              2007         0.8832876       0
9              2008         0.8790958       0
10             2009         0.8776745       0
11             2010         0.8680333       0
12             2011         0.8230752       0
13             mean         0.8747022      NA
14 mean_significant         0.8747022      NA


6. [feedset_Crops](feedcr) and  [FAO_NatAnimals](natani)
               year cross_correlation p_value
1              2000         0.6378422       0
2              2001         0.6446435       0
3              2002         0.6576372       0
4              2003         0.6583182       0
5              2004         0.6377162       0
6              2005         0.6479552       0
7              2006         0.6756981       0
8              2007         0.6777420       0
9              2008         0.6835996       0
10             2009         0.6910878       0
11             mean         0.6612240      NA
12 mean_significant         0.6612240      NA


7. [feedset_Animals](feedani) and  [FAO_NatAnimals](natani)
               year cross_correlation p_value
1              2000         0.6727232       0
2              2001         0.6371335       0
3              2002         0.6352843       0
4              2003         0.6203713       0
5              2004         0.6579711       0
6              2005         0.6879710       0
7              2006         0.6590260       0
8              2007         0.6728331       0
9              2008         0.6857233       0
10             2009         0.7063633       0
11             mean         0.6635400      NA
12 mean_significant         0.6635400      NA


8. [LivestockDensity](lstden) and [FAO_natani_lsdmals](natani_lsd) livestock take the sum
        AnimalType cross_correlation   p_value
1        Buffaloes         0.9992334 0.0000000
2           Cattle        -0.2401017 0.2012483
3            Goats         0.9857703 0.0000000
4             Pigs         0.9992462 0.0000000
5            Sheep         0.9798715 0.0000000
6          Chicken         0.9675194 0.0000000
7             mean         0.7819232        NA
8 mean_significant         0.9863281        NA

8. [LivestockDensity](lstden) and [FAO_natani_lsdmals](natani_lsd) livestock take the mean
        AnimalType cross_correlation    p_value
1        Buffaloes         0.9809870 0.00000000
2           Cattle        -0.2204419 0.24176998
3            Goats         0.9682505 0.00000000
4             Pigs         0.9981507 0.00000000
5            Sheep         0.4455036 0.01361753
6          Chicken         0.9675194 0.00000000
7             mean         0.6899949         NA
8 mean_significant         0.8720822         NA

9. [feedset_Crops](feedcr) and [feedset_Animals](feedani)
               year cross_correlation p_value
1              2000         0.8203225       0
2              2001         0.8132748       0
3              2002         0.8151677       0
4              2003         0.8033235       0
5              2004         0.8191128       0
6              2005         0.8176083       0
7              2006         0.8176392       0
8              2007         0.8072737       0
9              2008         0.8314049       0
10             2009         0.8222276       0
11             mean         0.8167355      NA
12 mean_significant         0.8167355      NA
